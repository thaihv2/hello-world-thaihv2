import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-trees',
  templateUrl: './trees.component.html',
  styleUrls: ['./trees.component.css']
})
export class TreesComponent implements OnInit {
  title: string = 'Tree Component';

  @Input() name: string;
  @Output() selected = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  click(): void {
    this.selected.emit(this.name);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreesComponent } from './trees.component';

describe('TreesComponent', () => {
  let component: TreesComponent;
  let fixture: ComponentFixture<TreesComponent>;
  let h1: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    h1 = fixture.nativeElement.querySelector('h1');
  });

  it('TreesComponent should create', () => {
    expect(component).toBeTruthy();
  });

  it('raises the selected event when clicked', () => {
    component.name = 'Ha Van Thai'
    component.selected.subscribe(selectedName => expect(selectedName).toBe('Ha Van Thai'));
    component.click();
  });

  it('h1 ...', () => {
    expect(h1.textContent).toEqual('Tree Component');
  });

});

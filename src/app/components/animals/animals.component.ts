import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  isOn: boolean = false;

  clicked(): void {
    this.isOn = !this.isOn;
  }
  
  get message() {
    return `The light is ${this.isOn ? 'On' : 'Off'}`
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { ValueService } from './value.service';

describe('ValueService', () => {
  let _service : ValueService;
  beforeEach(() => {
    _service = new ValueService();
  });

  it('ValueService should be created', inject([ValueService], (service: ValueService) => {
    expect(service).toBeTruthy();
  }));

  it('getValue should be return get-value',
    () => {
      expect(_service.getValue()).toBe('get-value');
    }
  );

  it('getObservableValue should return get-observable-value',
    (done: DoneFn) => { 
      _service.getObservableValue().subscribe(value => {
        expect(value).toBe('get-observable-value');
        done();
      })
    }
  );

  it('getPromiseValue should return get-promise-value',
    (done: DoneFn) => {
      _service.getPromiseValue().then(value => {
        expect(value).toBe('get-promise-value');
        done();
      })
    }
  );

});

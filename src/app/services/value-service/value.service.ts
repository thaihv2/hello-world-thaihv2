import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValueService {

  constructor() { }

  // Normal method
  getValue() : string {
    return "get-value";
  }

  // Observable method
  getObservableValue() : Observable<string> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next("get-observable-value");
        observer.complete();
      }, 3000);
    });
  }

  // Promise method
  getPromiseValue() : Promise<string> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve("get-promise-value");
      }, 2000);
    });
  }

  getValueWithId(id: number) : string{
    return id + ': get-value-with-id';
  }

}

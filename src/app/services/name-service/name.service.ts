import { Injectable } from '@angular/core';
import { ValueService } from '../value-service/value.service';

@Injectable({
  providedIn: 'root'
})
export class NameService {

  constructor(private _valueService : ValueService) { }

  getValue() : string {
    return this._valueService.getValue();
  }

  getValueWithId(id: number) : string {
    return this._valueService.getValue() + '-' + this._valueService.getValueWithId(id);
  }

  getName(name: string) : string {
    return 'name: ' + name;
  }

}

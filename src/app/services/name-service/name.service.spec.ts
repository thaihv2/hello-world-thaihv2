import { TestBed, inject } from '@angular/core/testing';

import { NameService } from './name.service';
import { ValueService } from '../value-service/value.service';

describe('NameService', () => {
  let _nameService : NameService;
  beforeEach(() => {
  });

  // real service
  it('NameService.getValue should return real value from the real service', () => {
    _nameService = new NameService(new ValueService())
    expect(_nameService.getValue()).toBe('get-value');
  });
  
  // fake service
  //...

  // fake object as service
  it('NameService.getValue should return real value from fake object', () => {
    const fake = { getValue: () => 'get-value' };
    _nameService = new NameService(fake as ValueService);
    expect(_nameService.getValue()).toBe('get-value');
  });

  // spy
  it('NameService.getValue should stubbed value from a spy', () => {
    const valueServiceSpy = jasmine.createSpyObj('ValueService', ['getValue']);
    const stubValue = 'get-value';
    valueServiceSpy.getValue.and.returnValue(stubValue);

    _nameService = new NameService(valueServiceSpy);

    expect(_nameService.getValue()).toBe('get-value', 'NameSerivce returned get-value');
    expect(valueServiceSpy.getValue.calls.count()).toBe(1, 'Spy method was called once');
    expect(valueServiceSpy.getValue.calls.mostRecent().returnValue).toBe(stubValue);
  })

  //spy with parameter
  it('NameService.getValueWithId should stubbed value from a spy', () => {
    const valueServiceSpy = jasmine.createSpyObj('ValueService', ['getValue', 'getValueWithId']);
    const id = 1;
    const stubGetValue = 'get-value';
    const stubGetValueWithId = id + ': get-value-with-id';
    valueServiceSpy.getValue.and.returnValue(stubGetValue);
    valueServiceSpy.getValueWithId.and.returnValue(stubGetValueWithId);
    
    _nameService = new NameService(valueServiceSpy);
    
    expect(_nameService.getValueWithId(id)).toBe('get-value-1: get-value-with-id', 'NameSerivce.getValueWithId returned ...');
  });

});

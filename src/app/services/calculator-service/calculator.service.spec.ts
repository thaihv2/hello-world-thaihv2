import { TestBed, inject } from '@angular/core/testing';

import { CalculatorService } from './calculator.service';
import { NameService } from '../name-service/name.service';

describe('CalculatorService', () => {
  let _calculatorService: CalculatorService;
  let _nameServiceSpy: jasmine.SpyObj<NameService>

  beforeEach(() => {
    const spy = jasmine.createSpyObj('NameService', ['getName']);

    TestBed.configureTestingModule({
      providers: [CalculatorService, { provide: NameService, useValue: spy }]
    });

    _calculatorService = TestBed.get(CalculatorService);
    _nameServiceSpy = TestBed.get(NameService);
  });

  function setup() {
    const spy = jasmine.createSpyObj('NameService', ['getName']);
    const stubName = 'havanthai';
    const calculatorService = new NameService(spy);
    spy.getName.and.returnValue('name: ' + stubName);
    return { calculatorService, stubName, spy };
  }

  it('CalculatorService.getFirstPrime should returned the first prime - 2', () => {
    const stubValue = 2;
    _nameServiceSpy.getName.and.returnValue(stubValue);

    expect(_calculatorService.getFirstPrime()).toBe(2, 'service returned 2');

  });

  it('CalculatorService.getName should returned name: havanthai', () => {
    const stubValue = 'havanthai';
    _nameServiceSpy.getName.and.returnValue('name: ' + stubValue);

    expect(_calculatorService.getName(stubValue)).toBe('name: ' + stubValue, 'service returned name: havanthai');
    expect(_nameServiceSpy.getName.calls.count()).toBe(1, 'spy method was called once');
    expect(_nameServiceSpy.getName.calls.mostRecent().returnValue).toBe('name: ' + stubValue);
  });

  //use setup()
  it('CalculatorService.getName should returned name: havanthai', () => {
    const { calculatorService, stubName, spy } = setup();
    
    expect(calculatorService.getName(stubName)).toBe('name: ' + stubName, 'service returned name: havanthai');
    // doesn't work???
    // expect(spy.getName.calls.count()).toBe(1, 'spy method was called once');
    // expect(spy.getName.calls.mostRecent().returnValue).toBe('name: ' + stubName);
  });


});

import { Injectable } from '@angular/core';
import { NameService } from '../name-service/name.service';
import { doesNotThrow } from 'assert';
import { DomSharedStylesHost } from '@angular/platform-browser/src/dom/shared_styles_host';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor(private _nameService: NameService) { }

  getName(name: string): string {
    return this._nameService.getName(name);
  }

  getFirstPrime(): number {
    return 2;
  }

  add(a: number, b: number): number {
    return a + b;
  }

}

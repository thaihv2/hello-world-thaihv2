import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AnimalsComponent } from './components/animals/animals.component';
import { TreesComponent } from './components/trees/trees.component';

@NgModule({
  declarations: [
    AppComponent,
    AnimalsComponent,
    TreesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

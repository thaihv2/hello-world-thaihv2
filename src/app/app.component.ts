import { Component } from '@angular/core';
import { ValueService } from './services/value-service/value.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private _valueService: ValueService){}
}
